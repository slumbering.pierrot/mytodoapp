import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  SHOW_ALL,
  SHOW_COMPLETED,
  SHOW_ACTIVE,
} from "../constants/TodoFilters";
import TodoItem from "./TodoItem";

const TodoList = () => {
  const getVisibilityFilter = useSelector((state) => state.visibilityFilter);
  const getTodos = useSelector((state) => state.todos);
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(() => {
    const filteredTodos = () => {
      switch (getVisibilityFilter) {
        case SHOW_ALL:
          return getTodos;
        case SHOW_COMPLETED:
          return getTodos.filter((t) => t.completed);
        case SHOW_ACTIVE:
          return getTodos.filter((t) => !t.completed);
        default:
          throw new Error("Unknown filter: " + getVisibilityFilter);
      }
    };

    setFilteredTodos(filteredTodos);
  }, [getTodos, getVisibilityFilter]);

  return (
    <ul className="todo-list">
      {filteredTodos.map((todo) => (
        <TodoItem key={todo.id} todo={todo} />
      ))}
    </ul>
  );
};

export default TodoList;
