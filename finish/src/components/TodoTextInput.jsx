import React, { useState } from "react";
import classnames from "classnames";

const TodoTextInput = ({
  onSave,
  text = "",
  placeholder,
  editing,
  newTodo,
}) => {
  const [textState, setTextState] = useState(text || "");

  function handleBlur(event) {
    !newTodo && onSave(event.target.value);
  }

  function handleSubmit(event) {
    const text = event.target.value.trim();
    if (event.which === 13) {
      onSave(text);
      if (newTodo) {
        setTextState("");
      }
    }
  }

  return (
    <input
      className={classnames({
        edit: editing,
        "new-todo": newTodo,
      })}
      type="text"
      placeholder={placeholder}
      autoFocus={true}
      value={textState}
      onBlur={handleBlur}
      onChange={(event) => setTextState(event.target.value)}
      onKeyDown={handleSubmit}
    />
  );
};

export default TodoTextInput;
