import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { completeAllTodos } from "../actions";
import Footer from "./Footer";
import TodoList from "./TodoList";

const MainSection = () => {
  const todos = useSelector((state) => state.todos);
  const dispatch = useDispatch();
  const [completedCount, setCompletedCount] = useState(0);

  useEffect(() => {
    function getCompletedTodoCount() {
      setCompletedCount(
        todos.reduce((count, todo) => (todo.completed ? count + 1 : count), 0)
      );
    }
    getCompletedTodoCount();
  }, [todos]);

  return (
    <section className="main">
      {!!todos.length && (
        <span>
          <input
            className="toggle-all"
            type="checkbox"
            checked={completedCount === todos.length}
            readOnly
          />
          <label onClick={() => dispatch(completeAllTodos())} />
        </span>
      )}
      <TodoList />
      {!!todos.length && (
        <Footer
          completedCount={completedCount}
          activeCount={todos.length - completedCount}
        />
      )}
    </section>
  );
};

export default MainSection;
