import React from "react";
import { useSelector, useDispatch } from "react-redux";
import classnames from "classnames";

import { setVisibilityFilter } from "../actions";

const FilterLink = ({ filter, children }) => {
  const visibilityFilter = useSelector((state) => state.visibilityFilter);
  const dispatch = useDispatch();

  return (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      className={classnames({ selected: filter === visibilityFilter })}
      style={{ cursor: "pointer" }}
      onClick={() => dispatch(setVisibilityFilter(filter))}
    >
      {children}
    </a>
  );
};

export default FilterLink;
