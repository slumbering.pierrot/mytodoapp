import React, { useState } from "react";
import classnames from "classnames";
import { useDispatch } from "react-redux";

import TodoTextInput from "./TodoTextInput";
import { deleteTodo, editTodo, completeTodo } from "../actions";

const TodoItem = ({ todo }) => {
  const [editing, setEditing] = useState(false);
  const dispatch = useDispatch();

  function handleSave(id, text) {
    text.length === 0 ? dispatch(deleteTodo(id)) : dispatch(editTodo(id, text));
    setEditing(false);
  }

  return (
    <li
      className={classnames({
        completed: todo.completed,
        editing: editing,
      })}
    >
      {editing ? (
        <TodoTextInput
          text={todo.text}
          editing={editing}
          onSave={(text) => handleSave(todo.id, text)}
        />
      ) : (
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={todo.completed}
            onChange={() => dispatch(completeTodo(todo.id))}
          />
          <label onDoubleClick={() => setEditing(true)}>{todo.text}</label>
          <button
            className="destroy"
            onClick={() => dispatch(deleteTodo(todo.id))}
          />
        </div>
      )}
    </li>
  );
};

export default TodoItem;
